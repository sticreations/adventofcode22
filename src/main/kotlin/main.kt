

data class Player(
    val stack: MutableList<Int>
)



fun main(args: Array<String>) {
    val p1 = Player(mutableListOf(9,2,6,3,1))
    val p2 = Player(mutableListOf(5,8,4,7,10))
    val pair = Pair<Player, Player>(p1, p2)


    playRound(pair, 1)
    playRound(pair, 2)




    println("Hello World!")
}

fun playRound(players : Pair<Player, Player>, round: Int) : Pair<Player, Player> {
    println("We are in Round $round")
    println("Player 1 has ${players.first.stack}")
    println("Player 2 has ${players.second.stack}")


    val card1 = players.first.stack[0]
    val card2 = players.second.stack[0]

    println("Player One Plays $card1")
    println("Player Two Plays $card2")

    if(card1 > card2) {
        players.first.stack.add(card1)
        players.first.stack.add(card2)
        println("Player One Wins the Round")
    } else {
        players.second.stack.add(card2)
        players.second.stack.add(card1)
        println("Player Two Wins the Round")
    }
    players.first.stack.removeAt(0)
    players.second.stack.removeAt(0)

    return players
}